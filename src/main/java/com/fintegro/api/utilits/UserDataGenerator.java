package com.fintegro.api.utilits;

import com.github.javafaker.Faker;

public class UserDataGenerator {

    private static Faker faker = new Faker();

    public static String getFakerFirstName(){
        return faker.name().firstName();
    }

    public static String getFakerEmailAddress(){ return faker.internet().emailAddress(); }

    public static String getFakerInvalidEmailAddressWithoutDog(){
        return faker.internet().emailAddress().replace("@", "");
    }

    public static String getFakerInvalidEmailAddressWithoutCom(){
        return faker.internet().emailAddress().replace(".", "");
    }

    public static String getFakerInvalidEmailAddressWithoutDogAndCom(){
        return faker.internet().emailAddress().replace("@", "").replace(".", "");
    }

    public static String getFakerPasswordByLength(int length){
        return faker.number().digits(length);
    }

    public static String getFakerRandomNumber(int length){
        return faker.number().digits(length);
    }

}
