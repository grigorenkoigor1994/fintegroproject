package com.fintegro.api.properties;

public enum PropertySource {
    URL("url.properties");

    public String sourceFile;

    public PropertySource[] getOptions(){
        return PropertySource.values();
    }

    PropertySource(String sourceFile) {
        this.sourceFile = sourceFile;
    }

}

