package com.fintegro.api.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import static com.fintegro.api.utilits.UserDataGenerator.*;
import static com.fintegro.api.utilits.UserDataGenerator.getFakerPasswordByLength;

@Getter
@Setter
@Accessors(chain = true)
public class CreateUserModel{

	@JsonProperty("password")
	private String password;

	@JsonProperty("name")
	private String name;

	@JsonProperty("email")
	private String email;

	public CreateUserModel() {
		this.password = getFakerPasswordByLength(5);
		this.name = getFakerFirstName() + getFakerRandomNumber(4);
		this.email = getFakerEmailAddress();
	}
}