package com.fintegro.api.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class RegisterUserResponseModel {

	@JsonProperty("birthday")
	private int birthday;

	@JsonProperty("password")
	private String password;

	@JsonProperty("date_start")
	private int dateStart;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("name")
	private String name;

	@JsonProperty("avatar")
	private String avatar;

	@JsonProperty("email")
	private String email;

	@JsonProperty("hobby")
	private String hobby;
}