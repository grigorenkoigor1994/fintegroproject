package com.fintegro.api.asserts.userAssert;

import com.fintegro.api.model.user.CreateUserModel;
import com.fintegro.api.model.user.RegisterUserResponseModel;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class UserAssert {

    public static void equalsActualUserDataWithExpectUserData(RegisterUserResponseModel actualUserData, CreateUserModel expectUserData){
        assertEquals(actualUserData.getName(), expectUserData.getName());
        assertNotEquals(actualUserData.getPassword(), expectUserData.getPassword());
        assertEquals(actualUserData.getBirthday(), 0);
        assertEquals(actualUserData.getEmail(), expectUserData.getEmail());
        assertEquals(actualUserData.getGender(), "");
        assertEquals(actualUserData.getDateStart(), 0);
        assertEquals(actualUserData.getHobby(), "");
    }

}
