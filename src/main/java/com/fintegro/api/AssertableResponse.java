package com.fintegro.api;

import com.fintegro.api.conditions.Condition;
import io.restassured.response.Response;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AssertableResponse {

    private Response response;

    public AssertableResponse shouldHave(Condition condition){
        condition.check(response);
        return this;
    }

    public String getBodyByPath(String path){
        return response.then().extract().path(path).toString();
    }
    public String getResponseJsonBody(){
        return response.body().print();
    }

    public <T> T responseAs(Class<T> tClass){
        return response.as(tClass);
    }
}

