package com.fintegro.api.service;

import com.fintegro.api.AssertableResponse;
import com.fintegro.api.model.user.CreateUserModel;
import io.restassured.response.Response;

public class UserApiService extends SetupApiService {

    public AssertableResponse registerUser(CreateUserModel createUserModel) {
        Response register =
                setup()
                        .body(createUserModel)
                        .when()
                        .post("doregister")
                        .then().extract().response();

        return new AssertableResponse(register);
    }
}
