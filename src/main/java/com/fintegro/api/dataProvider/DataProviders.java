package com.fintegro.api.dataProvider;


import org.testng.annotations.DataProvider;

import static com.fintegro.api.utilits.UserDataGenerator.*;

public class DataProviders {

    @DataProvider(name = "IncorrectEmailValidation")
    public static Object[][] incorrectEmail() {
        return new Object[][]{
                {""},
                {getFakerEmailAddress().replace("@", "")},
                {getFakerEmailAddress().replace(".", "")},
                {getFakerEmailAddress().replace("@", "").replace(".", "")}
        };
    }
}
