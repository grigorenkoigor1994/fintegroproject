package com.fintegro.tests;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeSuite;

import static com.fintegro.api.properties.UrlLinks.MAIN_URL;

public class BaseTest {



    @BeforeSuite
    public void preSuite(){
        RestAssured.baseURI = MAIN_URL;
    }

}
