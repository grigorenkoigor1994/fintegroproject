package com.fintegro.tests.userTests;

import com.fintegro.api.dataProvider.DataProviders;
import com.fintegro.api.model.user.CreateUserModel;
import com.fintegro.api.model.user.RegisterUserResponseModel;
import com.fintegro.api.service.UserApiService;
import com.fintegro.tests.BaseTest;
import org.testng.annotations.Test;

import static com.fintegro.api.asserts.userAssert.UserAssert.equalsActualUserDataWithExpectUserData;
import static com.fintegro.api.conditions.Conditions.bodyField;
import static com.fintegro.api.conditions.Conditions.statusCode;
import static com.fintegro.api.utilits.UserDataGenerator.*;
import static org.hamcrest.Matchers.containsString;

public class RegisterTest extends BaseTest {

    private UserApiService userApiService = new UserApiService();

    @Test(description = "Создание нового пользователя PR-1")
    public void registerNewUser() {
        CreateUserModel newUserModel = new CreateUserModel();

        RegisterUserResponseModel registerUserResponseModel = new RegisterUserResponseModel();

        registerUserResponseModel = userApiService.registerUser(newUserModel)
                .shouldHave(statusCode(200))
                .responseAs(RegisterUserResponseModel.class);

        equalsActualUserDataWithExpectUserData(registerUserResponseModel, newUserModel);
    }

    @Test(description = "Создание пользователя c использованной ранее почтой PR-2")
    public void registerNewUserWithExistingEmail() {
        CreateUserModel newUserModel = new CreateUserModel();

        userApiService.registerUser(newUserModel)
                .shouldHave(statusCode(200));

        CreateUserModel newUserWithDuplicateEmail = new CreateUserModel()
                .setEmail(newUserModel.getEmail());

        userApiService.registerUser(newUserWithDuplicateEmail)
                .shouldHave(statusCode(200))
                .shouldHave(bodyField("type", containsString("error")))
                .shouldHave(bodyField("message", containsString(" email " + newUserModel.getEmail() + " уже есть в базе")));
    }

    @Test(description = "Создание пользователя с невалидной почтой PR-3-4",
            dataProviderClass = DataProviders.class, dataProvider = "IncorrectEmailValidation")
    public void registerNewUserWithInvalidEmail(String email) {
        CreateUserModel newUserModel = new CreateUserModel()
                .setEmail(email);

        userApiService.registerUser(newUserModel)
                .shouldHave(statusCode(200))
                .shouldHave(bodyField("type", containsString("error")))
                .shouldHave(bodyField("message", containsString(" Некоректный  email " + newUserModel.getEmail())));
    }

    @Test(description = "Создание пользователя без имени PR-5")
    public void registerNewUserWithoutName() {
        CreateUserModel newUserModel = new CreateUserModel()
                .setName("");

        userApiService.registerUser(newUserModel)
                .shouldHave(statusCode(200))
                .shouldHave(bodyField("type", containsString("error")))
                .shouldHave(bodyField("message", containsString("поле фио является обязательным")));
    }


    @Test(description = "Создание пользователя c использованным ранее именем PR-6")
    public void registerNewUserWithExistingName() {
        CreateUserModel newUserModel = new CreateUserModel();

        userApiService.registerUser(newUserModel)
                .shouldHave(statusCode(200));

        CreateUserModel newUserWithDuplicateName = new CreateUserModel()
                .setName(newUserModel.getName());

        userApiService.registerUser(newUserWithDuplicateName)
                .shouldHave(statusCode(200))
                .shouldHave(bodyField("type", containsString("error")))
                .shouldHave(bodyField("message", containsString(" Текущее ФИО "+ newUserWithDuplicateName.getName() +" уже есть в базе")));

    }

    @Test(description = "Создание пользователя без пароля PR-7")
    public void registerNewUserWithoutPassword() {
        CreateUserModel newUserModel = new CreateUserModel()
                .setPassword("");

        userApiService.registerUser(newUserModel)
                .shouldHave(statusCode(200))
                .shouldHave(bodyField("type", containsString("error")))
                .shouldHave(bodyField("message", containsString("поле пароль является обязательным")));
    }

    @Test(description = "Создание пользователя c использованным ранее паролем PR-8")
    public void registerNewUserWithExistingPassword() {
        CreateUserModel newUserModel = new CreateUserModel();

        userApiService.registerUser(newUserModel)
                .shouldHave(statusCode(200));

        CreateUserModel newUserWithDuplicatePassword = new CreateUserModel()
                .setPassword(newUserModel.getPassword());

        RegisterUserResponseModel registerUserResponseModel = new RegisterUserResponseModel();

        registerUserResponseModel = userApiService.registerUser(newUserWithDuplicatePassword)
                .shouldHave(statusCode(200))
                .responseAs(RegisterUserResponseModel.class);

        equalsActualUserDataWithExpectUserData(registerUserResponseModel,newUserWithDuplicatePassword);
    }
}
